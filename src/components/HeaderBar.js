import React from 'react';
import './HeaderBar.css';
import Logo from '../assets/pictures/Logo42.svg'

class HeaderBar extends React.Component {
    render() {
        return <div>
            <div className="background">
                <img src={Logo} alt={"Logo"} className="logo"/>
                <h1 className="text">News42</h1>
            </div>

        </div>
    }
}

export default HeaderBar;