import React from 'react';
import Button from "@material-ui/core/es/Button/Button";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import NewsComponent from "./NewsComponent";
import List from "@material-ui/core/List/List";
import GridList from "@material-ui/core/GridList/GridList";
import GridListTile from "@material-ui/core/GridListTile/GridListTile";

class NewsFeed extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            news: [{name: 'tim'}]
        }
        this.fetchData = this.fetchData.bind(this);
    }

    fetchData() {
        let component = this
        axios
            .get("https://jsonplaceholder.typicode.com/posts")
            .then(function (response) {
                let fetchedData = response.data;
                component.setState(state => ({
                    news: fetchedData
                }));
                console.log(fetchedData)
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    render() {
        return <div>
            <Button onClick={this.fetchData}>FETCH DATA</Button>
            <GridList cellHeight={160} cols={2}>
                {this.state.news.map(singleNews => (
                    <NewsComponent key={singleNews.title} news={singleNews}/>
                ))}
            </GridList>
        </div>
    }

}

export default NewsFeed;

