import React from "react";

import ListItem from "@material-ui/core/es/ListItem/ListItem";
import ListItemText from "@material-ui/core/es/ListItemText/ListItemText";
import Card from "@material-ui/core/es/Card/Card";
import GridListTile from "@material-ui/core/GridListTile/GridListTile";


class NewsComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isClicked: false
        }
        this.showDetails = this.showDetails.bind(this);
    }

    body = this.isClicked ? <ListItemText xsdown>{this.props.news.body}</ListItemText> : null

    render() {
        this.key = this.props.news.title;

        return(
            <div>
            <GridListTile key={this.props.news.title} cols={this.props.cols || 1}>
                <ListItemText>{this.props.news.title}</ListItemText>
                <ListItemText xsdown>{this.props.news.body}</ListItemText>

            </GridListTile>

        <ListItem button onClick={this.showDetails}>
                <Card>
                <ListItemText>{this.props.news.title}</ListItemText>
                <ListItemText xsdown>{this.props.news.body}</ListItemText>
                </Card>
           </ListItem>
            </div>
        )

    }

    showDetails() {


        this.setState(state => ({isClicked: !this.state.isClicked}))

    }
}

export default NewsComponent;