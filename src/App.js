import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import {H1} from '@material-ui/core';
import HeaderBar from "./components/HeaderBar";
import NewsFeed from "./components/NewsFeed/NewsFeed";

function App() {
    return (
        <React.Fragment>
            <CssBaseline />

            <HeaderBar/>
            <NewsFeed/>
        </React.Fragment>
    );
}

export default App;
